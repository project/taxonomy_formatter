<?php

namespace Drupal\taxonomy_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'Taxonomy Term' formatter.
 *
 * @FieldFormatter(
 *  id = "taxonomy_term_reference_formatter",
 *  label = @Translation("Taxonomy Formatter"),
 *  field_types = {
 *   "entity_reference"
 *  }
 * )
 */
class TaxonomyTermReferenceFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'links_option' => FALSE,
      'separator_option' => ', ',
      'element_option' => '- None -',
      'wrapper_option' => '- None -',
      'element_class' => '',
      'wrapper_class' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['links_option'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Links'),
      '#description' => $this->t('When checked terms will be displayed as links.'),
      '#default_value' => $this->getSetting('links_option'),
    ];
    $element['separator_option'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Separator'),
      '#description' => $this->t('The separator to use, including leading and trailing spaces.'),
      '#default_value' => $this->getSetting('separator_option'),
    ];
    $element['element_option'] = [
      '#type' => 'select',
      '#title' => $this->t('Element'),
      '#description' => $this->t('The HTML element to wrap each tag in.'),
      '#default_value' => $this->getSetting('element_option'),
      '#options' => [
        '- None -' => $this->t('- None -'),
        'span' => $this->t('span'),
        'h1' => $this->t('h1'),
        'h2' => $this->t('h2'),
        'h3' => $this->t('h3'),
        'h4' => $this->t('h4'),
        'h5' => $this->t('h5'),
        'strong' => $this->t('h6'),
        'em' => $this->t('h7'),
      ],
    ];
    $element['element_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Element Class'),
      '#description' => $this->t('The class assigned to the element.'),
      '#default_value' => $this->getSetting('element_class'),
    ];
    $element['wrapper_option'] = [
      '#type' => 'select',
      '#title' => $this->t('Wrapper'),
      '#description' => $this->t('The HTML element to wrap the entire collection in.'),
      '#default_value' => $this->getSetting('wrapper_option'),
      '#options' => [
        '- None -' => $this->t('- None -'),
        'div' => $this->t('div'),
        'span' => $this->t('span'),
        'h1' => $this->t('h1'),
        'h2' => $this->t('h2'),
        'h3' => $this->t('h3'),
        'h4' => $this->t('h4'),
        'h5' => $this->t('h5'),
        'p' => $this->t('p'),
        'strong' => $this->t('strong'),
        'em' => $this->t('em'),
      ],
    ];
    $element['wrapper_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wrapper Class'),
      '#description' => $this->t('The class assigned to the wrapper.'),
      '#default_value' => $this->getSetting('wrapper_class'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('The Terms will be displayed separated by "@separator"', ['@separator' => $this->getSetting('separator_option')]);
    if ($this->getSetting('links_option')) {
      $summary[] = $this->t('<br>The terms will link to the term pages');
    }
    if ($this->getSetting('element_option') != "- None -") {
      $summary[] = $this->t('<br>Elements will be wrapped in a "@element" tag', ['@element' => $this->getSetting('element_option')]);
      if (!empty($this->getSetting('element_class'))) {
        $summary[] = ' ' . $this->t('with the class of @elemclass', ['@elemclass' => $this->getSetting('element_class')]);
      }
    }
    if ($this->getSetting('wrapper_option') != "- None -") {
      $summary[] = $this->t('<br>The entire list will be wrapped in a "@wrapper" tag', ['@wrapper' => $this->getSetting('wrapper_option')]);
      if (!empty($this->getSetting('wrapper_class'))) {
        $summary[] = ' ' . $this->t('with the class of @wrapclass', ['@wrapclass' => $this->getSetting('wrapper_class')]);
      }
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Don't output anything if there aren't any items.
    if (count($items) < 1) {
      return [];
    }

    $element = [];
    $separator = Html::escape($this->getSetting('separator_option'));
    if ($this->getSetting('element_option') != '- None -') {
      $elementwrap[0] = '<' . $this->getSetting('element_option') . ' class="' . Html::cleanCssIdentifier($this->getSetting('element_class')) . '">';
      $elementwrap[1] = '</' . $this->getSetting('element_option') . '>';
    }
    else {
      $elementwrap[0] = '';
      $elementwrap[1] = '';
    }
    if ($this->getSetting('wrapper_option') != '- None -') {
      $wrapper[0] = '<' . $this->getSetting('wrapper_option') . ' class="' . Html::cleanCssIdentifier($this->getSetting('wrapper_class')) . '">';
      $wrapper[1] = '</' . $this->getSetting('wrapper_option') . '>';
    }
    else {
      $wrapper[0] = '';
      $wrapper[1] = '';
    }
    $formatted = '';
    foreach ($this->getEntitiesToView($items, $langcode) as $entity) {
      if ($this->getSetting('links_option')) {
        $formatted .= $elementwrap[0] . $entity->toLink()
          ->toString() . $elementwrap[1] . $separator;
      }
      else {
        $formatted .= $elementwrap[0] . Html::escape($entity->label()) . $elementwrap[1] . $separator;
      }
    }
    $length = strlen($separator);
    $formatted = substr($formatted, 0, -($length));
    $formatted = $wrapper[0] . $formatted . $wrapper[1];
    $element[0]['#markup'] = $formatted;
    return $element;
  }

}
